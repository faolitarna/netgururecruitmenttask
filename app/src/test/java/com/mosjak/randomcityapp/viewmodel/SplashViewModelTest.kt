package com.mosjak.randomcityapp.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.mosjak.randomcityapp.CoroutinesTestRule
import com.mosjak.randomcityapp.data.model.city.EmissionData
import com.mosjak.randomcityapp.data.repository.CityRepository
import com.mosjak.randomcityapp.data.repository.EmitterRepository
import com.mosjak.randomcityapp.presentation.splash.SplashViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner.StrictStubs


@ExperimentalCoroutinesApi
@RunWith(value = StrictStubs::class)
class SplashViewModelTest {

  //region ViewModel

  private lateinit var viewModel: SplashViewModel

  @Mock
  private lateinit var cityRepository: CityRepository

  @Mock
  private lateinit var emitterRepository: EmitterRepository

  //endregion

  //region Rules

  @get:Rule
  val rule = InstantTaskExecutorRule()

  @get:Rule
  val coroutinesRule = CoroutinesTestRule()

  //endregion

  //region Setup

  companion object {
    private val EMISSION_DATA = EmissionData("Gdańsk", "Blue")
  }

  @Before
  fun setUp() {

    // Build view model.
    viewModel = SplashViewModel(cityRepository, emitterRepository)
  }

  //endregion

  //region First emission

  /**
   * After item was emitted, insert it to database.
   */
  @Test
  fun `Verify whether item was inserted to db after emission`() = runBlockingTest {

    // Given.
    Mockito
      .doReturn(flowOf(EMISSION_DATA))
      .`when`(emitterRepository).emit()

    // When.
    viewModel.startEmitting()

    // Then.
    Mockito.verify(cityRepository, Mockito.times(1)).insertItem(EMISSION_DATA)
  }

  //endregion
}
