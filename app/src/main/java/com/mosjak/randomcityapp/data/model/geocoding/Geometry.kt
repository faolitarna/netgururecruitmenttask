package com.mosjak.randomcityapp.data.model.geocoding

import com.google.gson.annotations.SerializedName

data class Geometry(
  @SerializedName("location")
  val location: LocationResponse
)
