package com.mosjak.randomcityapp.data.repository

import com.google.android.gms.maps.model.LatLng
import com.mosjak.randomcityapp.data.network.service.GeocodingApi

class GeocodingRepository(
  private val api: GeocodingApi
) {

  //region Get

  suspend fun getCoordinatesByCity(cityName: String, apiKey: String): LatLng {
    val response =
      api
        .getCoordinatesByCity(cityName, apiKey)
        .results
        .firstOrNull()
        ?.geometry
        ?.location

    val lat = response?.lat ?: 0.0
    val long = response?.lng ?: 0.0

    return LatLng(lat, long)
  }

  //endregion
}
