package com.mosjak.randomcityapp.data.model.geocoding

import com.google.gson.annotations.SerializedName

data class GeocodingResult(
  @SerializedName("geometry")
  val geometry: Geometry
)
