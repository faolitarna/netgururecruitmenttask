package com.mosjak.randomcityapp.data.model.city

import androidx.annotation.ColorInt
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.Date

@Entity(tableName = "city")
data class City(

  @PrimaryKey(autoGenerate = true)
  @ColumnInfo(name = "id")
  val id: Long,

  val name: String,

  @ColorInt
  val color: Int,

  val emissionDate: Date
)
