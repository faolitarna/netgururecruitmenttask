package com.mosjak.randomcityapp.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.mosjak.randomcityapp.data.db.converter.DateConverter
import com.mosjak.randomcityapp.data.model.city.City

@Database(
    version = 1,
    entities = [
      City::class
    ]
)
@TypeConverters(
    DateConverter::class
)
abstract class RandomCityDatabase : RoomDatabase() {

  //region Dao

  abstract fun cityDao(): CityDao

  //endregion
}
