package com.mosjak.randomcityapp.data.repository

import com.mosjak.randomcityapp.data.emitter.CityEmitter
import com.mosjak.randomcityapp.data.model.city.EmissionData
import kotlinx.coroutines.flow.Flow

class EmitterRepository(
  private val emitter: CityEmitter
) {

  //region Emit

  fun emit(): Flow<EmissionData> =
    emitter.emit()

  //endregion
}
