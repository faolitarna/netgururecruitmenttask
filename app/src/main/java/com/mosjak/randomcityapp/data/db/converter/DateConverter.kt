package com.mosjak.randomcityapp.data.db.converter

import androidx.room.TypeConverter
import java.util.Date

class DateConverter {

  //region Date

  @TypeConverter
  fun millisToDate(millis: Long?): Date {
    if (millis == null) return Date()

    return Date(millis)
  }

  @TypeConverter
  fun dateToMillis(date: Date): Long =
      date.time

  //endregion
}
