package com.mosjak.randomcityapp.data.repository

import androidx.lifecycle.LiveData
import com.mosjak.randomcityapp.data.db.CityDao
import com.mosjak.randomcityapp.data.model.city.City
import com.mosjak.randomcityapp.data.model.city.EmissionData
import com.mosjak.randomcityapp.presentation.common.color.ColorRetriever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.Date

class CityRepository(
  private val dao: CityDao,
  private val colorRetriever: ColorRetriever
) {

  //region Observe

  fun observeItems(): LiveData<List<City>> =
    dao.observeItems()

  //endregion

  //region Save

  suspend fun insertItem(item: EmissionData) = withContext(Dispatchers.IO) {

    val city = emissionItemToCity(item)

    dao.saveItem(city)
  }

  private fun emissionItemToCity(item: EmissionData): City =
    City(
      id = 0,
      name = item.cityName,
      colorRetriever.getColorByName(item.colorName),
      emissionDate = Date()
    )

  //endregion

  //region Clear

  suspend fun clear() =
    dao.clearTable()

  //endregion
}
