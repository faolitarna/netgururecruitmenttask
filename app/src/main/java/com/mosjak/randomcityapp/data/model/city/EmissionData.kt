package com.mosjak.randomcityapp.data.model.city


data class EmissionData(

    val cityName: String,

    val colorName: String,
)
