package com.mosjak.randomcityapp.data.model.geocoding

import com.google.gson.annotations.SerializedName

data class GeocodingResponse(
  @SerializedName("results")
  val results: List<GeocodingResult>
)
