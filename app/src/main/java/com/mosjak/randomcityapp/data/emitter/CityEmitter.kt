package com.mosjak.randomcityapp.data.emitter

import com.mosjak.randomcityapp.data.model.city.EmissionData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class CityEmitter {

  //region Emit

  fun emit(): Flow<EmissionData> =
    flow {

      // Create infinite stream.
      while (true) {

        val emissionData = EmissionData(
          CITIES.random(),
          COLORS.random()
        )

        delay(DELAY)
        emit(emissionData)
      }
    }.flowOn(Dispatchers.IO)

  //endregion

  //region Data

  companion object {

    // Delay in seconds.
    const val DELAY = 5_000L

    private val CITIES = listOf(
      "Gdańsk",
      "Warszawa",
      "Poznań",
      "Białystok",
      "Wrocław",
      "Katowice",
      "Kraków"
    )

    private val COLORS = listOf(
      "Yellow",
      "Green",
      "Blue",
      "Red",
      "Black",
      "White"
    )

  }

  //endregion
}
