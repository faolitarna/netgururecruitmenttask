package com.mosjak.randomcityapp.data.network.service

import com.mosjak.randomcityapp.data.model.geocoding.GeocodingResponse
import retrofit2.http.GET
import retrofit2.http.Query


interface GeocodingApi {

  //region Geocoding

  @GET("maps/api/geocode/json")
  suspend fun getCoordinatesByCity(
    @Query("address") cityName: String,
    @Query("key") apiKey: String
  ): GeocodingResponse

  //endregion
}
