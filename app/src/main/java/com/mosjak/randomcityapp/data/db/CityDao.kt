package com.mosjak.randomcityapp.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.mosjak.randomcityapp.data.model.city.City

@Dao
interface CityDao {

  //region Observe

  @Query(
    value = "SELECT * FROM city ORDER BY name"
  )
  fun observeItems(): LiveData<List<City>>

  //endregion

  //region Save

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  suspend fun saveItem(model: City)

  //endregion

  //region Clear

  @Query(
    value = "DELETE FROM city"
  )
  suspend fun clearTable()

  //endregion
}
