package com.mosjak.randomcityapp.common.di

import com.google.gson.Gson
import com.mosjak.randomcityapp.data.network.service.GeocodingApi
import okhttp3.OkHttpClient.Builder
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

val networkModule = module {

  //region Retrofit

  single {
    Retrofit.Builder()
      .baseUrl("https://maps.googleapis.com")
      .addConverterFactory(GsonConverterFactory.create(Gson()))
      .client(Builder().build())
      .build()
  }

  //endregion

  //region Api

  fun provideGeocodingApi(retrofit: Retrofit): GeocodingApi =
    retrofit.create()

  single {
    provideGeocodingApi(get())
  }

  //endregion
}
