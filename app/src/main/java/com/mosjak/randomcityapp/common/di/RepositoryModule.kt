package com.mosjak.randomcityapp.common.di

import com.mosjak.randomcityapp.data.emitter.CityEmitter
import com.mosjak.randomcityapp.data.repository.CityRepository
import com.mosjak.randomcityapp.data.repository.EmitterRepository
import com.mosjak.randomcityapp.data.repository.GeocodingRepository
import org.koin.dsl.module

val repositoryModule = module {

  //region Repositories

  single { CityEmitter() }
  single { CityRepository(get(), get()) }
  single { EmitterRepository(get()) }
  single { GeocodingRepository(get()) }

  //endregion
}
