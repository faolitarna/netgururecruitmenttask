package com.mosjak.randomcityapp.common.di

import androidx.room.Room
import com.mosjak.randomcityapp.data.db.CityDao
import com.mosjak.randomcityapp.data.db.RandomCityDatabase
import org.koin.dsl.module

val databaseModule = module {

  //region Database

  single {
    Room
      .databaseBuilder(get(), RandomCityDatabase::class.java, "RandomCity.db")
      .fallbackToDestructiveMigration()
      .build()
  }

  //endregion

  //region Dao

  fun provideCityDao(db: RandomCityDatabase): CityDao =
    db.cityDao()

  single {
    provideCityDao(get())
  }

  //endregion
}
