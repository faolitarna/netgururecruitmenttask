package com.mosjak.randomcityapp.common.di

import com.mosjak.randomcityapp.presentation.common.color.ColorRetriever
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val utilsModule = module {

  //region Utils

  single { ColorRetriever(get()) }

  //endregion
}
