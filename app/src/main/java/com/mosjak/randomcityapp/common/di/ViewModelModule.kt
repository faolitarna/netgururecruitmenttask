package com.mosjak.randomcityapp.common.di

import com.mosjak.randomcityapp.presentation.main.MainViewModel
import com.mosjak.randomcityapp.presentation.main.common.SelectedCityViewModel
import com.mosjak.randomcityapp.presentation.main.detail.DetailsViewModel
import com.mosjak.randomcityapp.presentation.main.list.ListViewModel
import com.mosjak.randomcityapp.presentation.splash.SplashViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

  //region View Models

  viewModel { SplashViewModel(get(), get()) }
  viewModel { MainViewModel(get(), get()) }
  viewModel { SelectedCityViewModel() }
  viewModel { ListViewModel(get()) }
  viewModel { DetailsViewModel(get()) }

  //endregion
}
