package com.mosjak.randomcityapp.presentation.main

import com.mosjak.randomcityapp.data.model.city.EmissionData
import com.mosjak.randomcityapp.data.repository.CityRepository
import com.mosjak.randomcityapp.data.repository.EmitterRepository
import com.mosjak.randomcityapp.presentation.common.base.EmitterViewModel

class MainViewModel(
  private val cityRepository: CityRepository,
  emitterRepository: EmitterRepository
) : EmitterViewModel(emitterRepository) {

  //region Emission

  override suspend fun collectItem(data: EmissionData) {
    super.collectItem(data)

    // Insert item to db.
    cityRepository.insertItem(data)
  }

  //endregion
}
