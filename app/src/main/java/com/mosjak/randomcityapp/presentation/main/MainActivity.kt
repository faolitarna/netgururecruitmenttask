package com.mosjak.randomcityapp.presentation.main

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.mosjak.randomcityapp.NavGraphDirections
import com.mosjak.randomcityapp.R
import com.mosjak.randomcityapp.databinding.ActivityMainBinding
import com.mosjak.randomcityapp.presentation.main.common.SelectedCityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity() {

  //region View Model

  private val viewModel: MainViewModel
    by viewModel()

  private val cityViewModel: SelectedCityViewModel
    by viewModel()

  //endregion

  //region Binding

  private lateinit var binding: ActivityMainBinding

  //endregion

  //region Lifecycle

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    // Configure layout.
    binding = DataBindingUtil
      .setContentView(this, R.layout.activity_main, null)

    observeCitySelected()
    initializeNavigationController()
    addDetailsFragmentConditionally()
  }

  override fun onStart() {
    super.onStart()

    viewModel.startEmitting()
  }

  override fun onPause() {
    super.onPause()

    viewModel.stopEmitting()
  }

  //endregion

  //region City

  private fun observeCitySelected() {
    cityViewModel
      .detailsRequested
      .observe(this) { openDetailsFragment() }
  }

  //endregion

  //region Navigation Controller

  private var navigationController: NavController? = null

  private fun initializeNavigationController() {

    // Search for navigation host fragment.
    val host = supportFragmentManager
      .findFragmentById(R.id.mainFragment) ?: return

    // Find nav controller.
    navigationController =
      (host as NavHostFragment).navController

    popDetailsFragmentIfIsDuplicated()
  }

  //endregion

  //region Navigation

  private fun openDetailsFragment() {

    if (isLandscapeTablet()) {
      addDetailsFragment()
      return
    }

    navigationController
      ?.navigate(NavGraphDirections.actionDetails())
  }

  private fun popDetailsFragmentIfIsDuplicated() {
    if (isLandscapeTablet() && isDetailsOnTop())
      navigationController?.popBackStack()
  }

  private fun isDetailsOnTop(): Boolean =
    navigationController?.currentDestination?.id == R.id.detailsFragment

  /**
   * Add details fragment if city is selected and device is tablet in landscape mode.
   */
  private fun addDetailsFragmentConditionally() =
    cityViewModel.model.value?.let {
      if (isLandscapeTablet()) addDetailsFragment()
    }

  private fun addDetailsFragment() {
    binding.detailsFragment?.visibility = View.VISIBLE
    binding.guideline?.setGuidelinePercent(0.4f)
  }

  private fun isLandscapeTablet(): Boolean =
    resources.getBoolean(R.bool.isLandscapeTablet)

  //endregion
}
