package com.mosjak.randomcityapp.presentation.main.list.item

import android.view.View
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.listeners.ClickEventHook
import com.mosjak.randomcityapp.data.model.city.City
import com.mosjak.randomcityapp.databinding.ItemCityBinding
import com.mosjak.randomcityapp.presentation.common.binding.asBinding

class CityItemEventHook(
  private val listener: (City) -> Unit
) : ClickEventHook<CityItem>() {

  //region Bind

  override fun onBind(viewHolder: ViewHolder): View? =
    viewHolder
      .asBinding<ItemCityBinding>()
      ?.cityView

  //endregion

  //region Click

  override fun onClick(
    v: View, position: Int, fastAdapter: FastAdapter<CityItem>,
    item: CityItem
  ) {

    // Invoke listener.
    listener.invoke(item.model)
  }

  //endregion
}

