package com.mosjak.randomcityapp.presentation.main.detail

import android.os.Bundle
import android.view.View
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat.getColor
import androidx.fragment.app.activityViewModels
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.mosjak.randomcityapp.R
import com.mosjak.randomcityapp.data.model.city.City
import com.mosjak.randomcityapp.databinding.FragmentDetailsBinding
import com.mosjak.randomcityapp.presentation.common.base.BaseFragment
import com.mosjak.randomcityapp.presentation.main.common.SelectedCityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class DetailsFragment : BaseFragment<FragmentDetailsBinding, DetailsViewModel>(),
  OnMapReadyCallback {

  //region Ui

  override val layoutRes: Int
    get() = R.layout.fragment_details

  //endregion

  //region View Model

  override val viewModel: DetailsViewModel
    by viewModel()

  private val cityViewModel: SelectedCityViewModel
    by activityViewModels()

  //endregion

  //region Binding

  override fun bindView(binding: FragmentDetailsBinding) {
    binding
      .also { it.viewModel = cityViewModel }
  }

  //endregion

  //region Lifecycle

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    setupToolbar()

    // Setup map.
    mapView = childFragmentManager
      .findFragmentById(R.id.mapView) as SupportMapFragment?

    getLocation()
    observeGetLocationCompleted()
  }

  //endregion

  //region Toolbar

  private fun setupToolbar() {
    cityViewModel
      .color
      .observe(viewLifecycleOwner, ::handleColorChanged)
  }

  private fun handleColorChanged(@ColorInt color: Int) = with(binding.toolbarView) {

    setBackgroundColor(color)

    /**
     * It requires better handling because black on a dark grayish background
     * could still look unreadable. Assuming we support only 6 colors it would be pretty easy to implement.
     * But if we'd like to support all possible colors it's not so trivial anymore.
     * Handling this edge case is out of the scope of this task in my opinion.
     */

    // Handle case when background is set to black same as text color.
    setTitleTextColor(getInvertedColor(color))
  }

  @ColorInt
  private fun getInvertedColor(@ColorInt color: Int): Int =
    when (color) {
      getColor(R.color.black) -> getColor(R.color.white)
      else -> getColor(R.color.black)
    }

  @ColorInt
  private fun getColor(@ColorRes color: Int) =
    getColor(requireContext(), color)

  //endregion

  //region Map

  private var mapView: SupportMapFragment? = null

  companion object {
    const val ZOOM_LEVEL = 10f
  }

  override fun onMapReady(map: GoogleMap?) {

    viewModel.location.value?.let {
      map?.moveCamera(CameraUpdateFactory.newLatLngZoom(it, ZOOM_LEVEL))
    }
  }

  private fun getLocation() {
    cityViewModel
      .model
      .observe(viewLifecycleOwner, ::handleCitySelected)
  }

  private fun handleCitySelected(city: City?) {

    // Get location long and lat of the city.
    city?.let {
      viewModel.getLocation(it, getString(R.string.google_maps_key))
    }
  }

  private fun observeGetLocationCompleted() {
    viewModel
      .location
      .observe(viewLifecycleOwner) { handleLocationCompleted() }
  }

  private fun handleLocationCompleted() {
    mapView?.getMapAsync(this)
  }

  //endregion
}
