package com.mosjak.randomcityapp.presentation.main.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.maps.model.LatLng
import com.mosjak.randomcityapp.data.model.city.City
import com.mosjak.randomcityapp.data.repository.GeocodingRepository
import com.mosjak.randomcityapp.presentation.common.SingleLiveEvent
import kotlinx.coroutines.launch

class DetailsViewModel(
  private val geocodingRepository: GeocodingRepository
) : ViewModel() {

  //region Location

  private val _location: MutableLiveData<LatLng> =
    SingleLiveEvent()

  internal val location: LiveData<LatLng> =
    _location

  fun getLocation(model: City, apiKey: String) {
    viewModelScope.launch {

      val location = geocodingRepository
        .getCoordinatesByCity(model.name, apiKey)

      _location.postValue(location)
    }
  }

  //endregion
}
