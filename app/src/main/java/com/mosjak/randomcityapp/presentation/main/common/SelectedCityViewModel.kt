package com.mosjak.randomcityapp.presentation.main.common

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import com.mosjak.randomcityapp.data.model.city.City
import com.mosjak.randomcityapp.presentation.common.SingleLiveEvent

class SelectedCityViewModel : ViewModel() {

  //region City

  private val _model: MutableLiveData<City> =
    MutableLiveData()

  val model: LiveData<City?> =
    _model

  val title: LiveData<String> =
    _model.map { it.name }

  val color: LiveData<Int> =
    _model.map { it.color }

  //endregion

  //region Select

  private val _detailsRequested: SingleLiveEvent<Any> =
    SingleLiveEvent()

  internal val detailsRequested: LiveData<Any> =
    _detailsRequested

  fun onCitySelected(city: City) {
    _detailsRequested.post()
    _model.postValue(city)
  }

  //endregion
}
