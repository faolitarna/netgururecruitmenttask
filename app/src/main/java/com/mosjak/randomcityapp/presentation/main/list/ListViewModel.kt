package com.mosjak.randomcityapp.presentation.main.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.mosjak.randomcityapp.data.model.city.City
import com.mosjak.randomcityapp.data.repository.CityRepository

class ListViewModel(
  repository: CityRepository
) : ViewModel() {

  //region List

  val items: LiveData<List<City>> =
    repository.observeItems()

  //endregion
}
