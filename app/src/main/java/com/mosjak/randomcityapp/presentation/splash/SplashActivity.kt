package com.mosjak.randomcityapp.presentation.splash

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mosjak.randomcityapp.R
import com.mosjak.randomcityapp.presentation.main.MainActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class SplashActivity : AppCompatActivity() {

  //region View Model

  private val viewModel: SplashViewModel
    by viewModel()

  //endregion

  //region Lifecycle

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_splash)

    observeFirstItemEmitted()
  }

  override fun onStart() {
    super.onStart()

    viewModel.startEmitting()
  }

  override fun onPause() {
    super.onPause()

    viewModel.stopEmitting()
  }

  //endregion

  //region Navigation

  private fun observeFirstItemEmitted() {

    viewModel
      .firstItemEmitted
      .observe(this) { navigateToMain() }
  }

  private fun navigateToMain() {
    Intent(this, MainActivity::class.java)
      .also { startActivity(it) }
  }

  //endregion
}
