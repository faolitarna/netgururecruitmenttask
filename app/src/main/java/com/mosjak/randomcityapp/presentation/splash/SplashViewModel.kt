package com.mosjak.randomcityapp.presentation.splash

import androidx.lifecycle.LiveData
import com.mosjak.randomcityapp.data.model.city.EmissionData
import com.mosjak.randomcityapp.data.repository.CityRepository
import com.mosjak.randomcityapp.data.repository.EmitterRepository
import com.mosjak.randomcityapp.presentation.common.SingleLiveEvent
import com.mosjak.randomcityapp.presentation.common.base.EmitterViewModel

class SplashViewModel(
  private val cityRepository: CityRepository,
  emitterRepository: EmitterRepository
) : EmitterViewModel(emitterRepository) {

  //region Emission

  override suspend fun collectItem(data: EmissionData) {
    super.collectItem(data)

    // Insert item to db and inform fragment.
    cityRepository.insertItem(data)
    _firstItemEmitted.post()
  }

  //endregion

  //region First Item

  private val _firstItemEmitted: SingleLiveEvent<Any> =
    SingleLiveEvent()

  internal val firstItemEmitted: LiveData<Any> =
    _firstItemEmitted

  //endregion
}
