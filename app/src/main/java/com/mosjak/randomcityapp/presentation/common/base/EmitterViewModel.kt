package com.mosjak.randomcityapp.presentation.common.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mosjak.randomcityapp.data.model.city.EmissionData
import com.mosjak.randomcityapp.data.repository.EmitterRepository
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import timber.log.Timber

abstract class EmitterViewModel(
  private val emitterRepository: EmitterRepository
) : ViewModel() {

  //region Emission

  private var emittingJob: Job? = null

  fun startEmitting() {
    Timber.v("Emission was started.")
    emittingJob = getCollectJob()
  }

  private fun getCollectJob(): Job =
    viewModelScope.launch {

      emitterRepository
        .emit()
        .collect(::collectItem)
    }

  protected open suspend fun collectItem(data: EmissionData) {
    Timber.v("New item emitted:")
    Timber.v(data.toString())
  }

  fun stopEmitting() {
    Timber.v("Emission was stopped.")
    emittingJob?.cancel()
  }

  //endregion
}
