package com.mosjak.randomcityapp.presentation.common.color;

import android.content.Context;
import android.content.res.Resources;
import androidx.annotation.ColorInt;
import androidx.core.content.ContextCompat;
import com.mosjak.randomcityapp.R;
import timber.log.Timber;

// Example java class.
public class ColorRetriever {

  //region Context

  Context context;

  public ColorRetriever(Context context) {
    this.context = context;
  }

  //endregion

  //region Retrieve

  public @ColorInt int getColorByName(String colorName) {

    // Resource should be in lower case.
    colorName = colorName.toLowerCase();

    Resources resources = context.getResources();
    String packageName = context.getPackageName();

    int colorId = resources
      .getIdentifier(colorName, "color", packageName);

    // If there is no such color, use default (black) one.
    int retrievedColor = ContextCompat.getColor(context, R.color.black);

    try {
      retrievedColor = ContextCompat.getColor(context, colorId);
    } catch (Exception exception) {
      Timber.e("Exception occurred during fetching color.");
      Timber.e(exception);
    }

    return retrievedColor;
  }

  //endregion
}


