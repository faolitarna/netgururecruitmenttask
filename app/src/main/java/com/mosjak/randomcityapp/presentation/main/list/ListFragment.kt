package com.mosjak.randomcityapp.presentation.main.list

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import com.mikepenz.fastadapter.adapters.GenericFastItemAdapter
import com.mosjak.randomcityapp.R
import com.mosjak.randomcityapp.data.model.city.City
import com.mosjak.randomcityapp.databinding.FragmentListBinding
import com.mosjak.randomcityapp.presentation.common.base.BaseFragment
import com.mosjak.randomcityapp.presentation.main.common.SelectedCityViewModel
import com.mosjak.randomcityapp.presentation.main.list.item.CityItem
import com.mosjak.randomcityapp.presentation.main.list.item.CityItemEventHook
import org.koin.androidx.viewmodel.ext.android.viewModel


class ListFragment : BaseFragment<FragmentListBinding, ListViewModel>() {

  //region Ui

  override val layoutRes: Int
    get() = R.layout.fragment_list

  //endregion

  //region View Model

  override val viewModel: ListViewModel
    by viewModel()

  private val cityViewModel: SelectedCityViewModel
    by activityViewModels()

  //endregion

  //region Binding

  override fun bindView(binding: FragmentListBinding) {
    // No-op by design.
  }

  //endregion

  //region Lifecycle

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    initializeRecyclerView()
    initializeEventHooks()

    observeItems()
  }

  //endregion

  //region Item

  private fun List<City>.items(): List<CityItem> =
    map(::CityItem)

  private val itemAdapter: GenericFastItemAdapter
    by lazy { GenericFastItemAdapter() }

  private fun showItems(result: List<City>) {

    itemAdapter.setNewList(result.items())
  }

  private fun observeItems() {

    viewModel
      .items
      .observe(viewLifecycleOwner, ::showItems)
  }

  //endregion

  //region Recycler View

  private fun initializeRecyclerView() = with(binding.citiesRecyclerView) {

    // Assign adapter to recycler view.
    adapter = itemAdapter
  }

  //endregion

  //region Event Hook

  private fun initializeEventHooks() {

    val eventHook = CityItemEventHook(cityViewModel::onCitySelected)

    // Initialize event hooks.
    itemAdapter
      .addEventHook(eventHook)
  }

  //endregion
}
