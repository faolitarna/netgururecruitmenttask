package com.mosjak.randomcityapp.presentation

import android.app.Application
import com.mosjak.randomcityapp.BuildConfig
import com.mosjak.randomcityapp.common.di.databaseModule
import com.mosjak.randomcityapp.common.di.networkModule
import com.mosjak.randomcityapp.common.di.repositoryModule
import com.mosjak.randomcityapp.common.di.utilsModule
import com.mosjak.randomcityapp.common.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.module.Module
import timber.log.Timber

class App : Application() {

  //region Lifecycle

  override fun onCreate() {
    super.onCreate()

    initializeLogger()
    initializeDependencyInjection()
  }

  //endregion

  //region Logger

  private fun initializeLogger() {

    // Conditionally enable logger.
    if (BuildConfig.DEBUG) {
      Timber.plant(Timber.DebugTree())
    }
  }

  //endregion

  //region Dependency Injection

  private fun initializeDependencyInjection() {

    startKoin {
      androidContext(this@App)

      modules(getListOfModules())
    }
  }

  private fun getListOfModules(): List<Module> =
    listOf(
      databaseModule,
      networkModule,
      repositoryModule,
      utilsModule,
      viewModelModule
    )

  //endregion
}
