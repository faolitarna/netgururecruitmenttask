package com.mosjak.randomcityapp.presentation.main.list.item

import android.view.LayoutInflater
import android.view.ViewGroup
import com.mikepenz.fastadapter.binding.ModelAbstractBindingItem
import com.mosjak.randomcityapp.R
import com.mosjak.randomcityapp.data.model.city.City
import com.mosjak.randomcityapp.databinding.ItemCityBinding

class CityItem(model: City) : ModelAbstractBindingItem<City, ItemCityBinding>(
  model
) {

  //region Initialization

  init {

    // Define identifier.
    identifier = model.id
  }

  //endregion

  //region Type

  override val type: Int
    get() = R.id.itemCity

  //endregion

  //region Binding

  override fun createBinding(inflater: LayoutInflater, parent: ViewGroup?)
    : ItemCityBinding = ItemCityBinding.inflate(inflater, parent, false)

  override fun bindView(binding: ItemCityBinding, payloads: List<Any>) {
    super.bindView(binding, payloads)

    // Update binding.
    updateCityTextBinding(binding)
    updateDateTextBinding(binding)
  }


  //endregion

  //region City Text

  private fun updateCityTextBinding(binding: ItemCityBinding) = with(binding.cityText) {

    text = model.name
    setTextColor(model.color)
  }

  //endregion

  //region Date

  private fun updateDateTextBinding(binding: ItemCityBinding) = with(binding.dateText) {

    text = model.emissionDate.toString()
  }

  //endregion
}
